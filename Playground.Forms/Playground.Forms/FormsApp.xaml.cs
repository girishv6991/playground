﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Playground.Forms.UI
{
	public partial class FormsApp : Application
	{
		public FormsApp ()
		{
			InitializeComponent();
		}

		protected override void OnStart ()
		{
            base.OnStart();
        }

		protected override void OnSleep ()
		{
            base.OnSleep();
		}

		protected override void OnResume ()
		{
            base.OnResume();
		}
	}
}
